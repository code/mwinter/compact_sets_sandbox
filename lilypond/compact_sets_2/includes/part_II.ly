{  
 { dis'1^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 }
 \bar "|"  
 { r4 d'2.^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { d'2. ~ d'8[ r8] }
 \bar "|"  
 { r2.  r8[ dis'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. ~ dis'8[ r8] }
 \bar "|"  
 { r2  r8[ d'8^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ d'4 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'8[ r8] r2.  }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. r4 }
 \bar "|"  
 { r4 r8[ cis'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ cis'2 ~ }
 \bar "|"  
 { cis'2. r8[ d'8^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 ~ d'8[ r8] c'2^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2. r4 }
 \bar "|"  
 { r8[ d'8^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ d'2. ~ }
 \bar "|"  
 { d'2 r8[ c'8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 r2.  }
 \bar "|"  
 { r8[ d'8^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ d'2. ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2. r8[ c'8^\markup { \pad-markup #0.2 "+28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 r2  }
 \bar "|"  
 { r4 r8[ cis'8^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ cis'2 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2 r2  }
 \bar "|"  
 { d'1^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { d'4 r8[ cis'8^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ cis'2 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'8[ r8] b2.^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 r2  }
 \bar "|"  
 { r2  cis'2^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { r8[ b8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ b2. ~ }
 \bar "|"  
 { b2 ~ b8[ r8] c'4^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 r4 r8[ cis'8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ cis'4 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2 r8[ b8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ b4 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b4 r4 b2^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b8[ r8] r4 }
 \bar "|"  
 { r4 r8[ b8^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ b2 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2. ~ b8[ r8] }
 \bar "|"  
 { c'1^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { c'2. ~ c'8[ r8] }
 \bar "|"  
 { r2  r8[ d'8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ d'4 ~ }
 \bar "|"  
 { d'1 }
 \bar "|"  
 { r8[ cis'8^\markup { \pad-markup #0.2 "-26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ cis'2. ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 ~ cis'8[ r8] r2  }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { b4 ~ b8[ r8] ais2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. r4 }
 \bar "|"  
 { r2  ais2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { r8[ ais8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ ais2. ~ }
 \bar "|"  
 { ais2 ~ ais8[ r8] b4^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b8[ r8] r4 }
 \bar "|"  
 { r4 r8[ c'8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ c'2 ~ }
 \bar "|"  
 { c'2 ~ c'8[ r8] b4^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 r2  }
 \bar "|"  
 { r2  b2^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b8[ r8] ais4^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 r8[ c'8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ c'4 ~ }
 \bar "|"  
 { c'2. ~ c'8[ r8] }
 \bar "|"  
 { ais1^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. ~ ais8[ r8] }
 \bar "|"  
 { r4 r8[ c'8^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ c'2 ~ }
 \bar "|"  
 { c'2. ~ c'8[ r8] }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { cis'2 ~ cis'8[ r8] r4 }
 \bar "|"  
 { r2  r8[ b8^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ b4 ~ }
 \bar "|"  
 { b2. r8[ cis'8^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'8[ r8] r4 b2^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { b2 ~ b8[ r8] c'4^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 r8[ ais8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ ais2 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 r8[ ais8^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. ~ ais8[ r8] }
 \bar "|"  
 { r2.  ais4^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { r8[ c'8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ c'2. ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] r2  }
 \bar "|"  
 { r8[ c'8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }}] ~ c'2. ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2. r4 }
 \bar "|"  
 { r8[ b8^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ b2. ~ }
 \bar "|"  
 { b2 r8[ c'8^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 r8[ ais8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 r2  }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b2 r8[ ais8^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. r4 }
 \bar "|"  
 { r4 r8[ c'8^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ c'2 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2. ~ c'8[ r8] }
 \bar "|"  
 { r4 r8[ c'8^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }}] ~ c'2 ~ }
 \bar "|"  
 { c'2 r8[ c'8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 r2  cis'4^\markup { \pad-markup #0.2 "+2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 ~ cis'8[ r8] r2  }
 \bar "|"  
 { c'1^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { c'2. ~ c'8[ r8] }
 \bar "|"  
 { r2  r8[ c'8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'8[ r8] d'2.^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'8[ r8] r4 dis'2^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 r2.  }
 \bar "|"  
 { r8[ d'8^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ d'2. ~ }
 \bar "|"  
 { d'2 ~ d'8[ r8] c'4^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2. r4 }
 \bar "|"  
 { r2  d'2^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { d'1 }
 \bar "|"  
 { r8[ c'8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ c'2. ~ }
 \bar "|"  
 { c'2 ~ c'8[ r8] cis'4^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r2.  }
 \bar "|"  
 { c'1^\markup { \pad-markup #0.2 "-3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] b2^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { b2. ~ b8[ r8] }
 \bar "|"  
 { c'1^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 ~ c'8[ r8] r4 }
 \bar "|"  
 { r2  c'2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'8[ r8] r2  b4^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b8[ r8] ais2.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { ais2 r8[ b8^\markup { \pad-markup #0.2 "-26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ b4 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 }
 \bar "|"  
 { r2  c'2^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 r2  }
 \bar "|"  
 { r4 c'2.^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'8[ r8] cis'2.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r2.  }
 \bar "|"  
 { c'1^\markup { \pad-markup #0.2 "+15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 r8[ ais8^\markup { \pad-markup #0.2 "+50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ ais2 ~ }
 \bar "|"  
 { ais2. r8[ ais8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 ~ ais8[ r8] r4 }
 \bar "|"  
 { r4 ais2.^\markup { \pad-markup #0.2 "+50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 r8[ c'8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ c'2 ~ }
 \bar "|"  
 { c'2 r8[ ais8^\markup { \pad-markup #0.2 "+50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 r2  r8[ c'8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] ais2^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { r8[ ais8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ ais2. ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { r2.  r8[ ais8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 ~ ais8[ r8] r4 }
 \bar "|"  
 { r4 r8[ b8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ b2 ~ }
 \bar "|"  
 { b1 }
 \bar "|"  
 { r8[ c'8^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ c'2. ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] b2^\markup { \pad-markup #0.2 "+8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { b2. ~ b8[ r8] }
 \bar "|"  
 { r1  }
 \bar "|"  
 { ais1^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { ais4 r8[ b8^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ b2 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 }
 \bar "|"  
 { r8[ c'8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ c'2. ~ }
 \bar "|"  
 { c'2 ~ c'8[ r8] cis'4^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2 ~ cis'8[ r8] r4 }
 \bar "|"  
 { c'1^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] r2  }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. r8[ c'8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2. ~ c'8[ r8] }
 \bar "|"  
 { r2  r8[ cis'8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ cis'4 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { r4 r8[ cis'8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ cis'2 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r8[ cis'8^\markup { \pad-markup #0.2 "+26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ cis'2 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2 ~ cis'8[ r8] r4 }
 \bar "|"  
 { r2  c'2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 ~ c'8[ r8] r4 }
 \bar "|"  
 { r4 c'2.^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] b2^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { b2. r8[ c'8^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 r2  d'4^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 r8[ c'8^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ c'2 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2. r8[ cis'8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r4 r8[ c'8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 }
 \bar "|"  
 { r8[ b8^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ b2. ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b4 r2  r8[ a8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2 r8[ ais8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 ~ ais8[ r8] c'2^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { c'2. r8[ b8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 r4 r8[ b8^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b4 r8[ ais8^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ ais2 ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { r8[ ais8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ ais2. ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { r8[ a8^\markup { \pad-markup #0.2 "+15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ a2. ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a4 r2  r8[ a8^\markup { \pad-markup #0.2 "+15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2. ~ a8[ r8] }
 \bar "|"  
 { r2  b2^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2. r4 }
 \bar "|"  
 { r4 r8[ ais8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ ais2 ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { r8[ b8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ b2. ~ }
 \bar "|"  
 { b4 ~ b8[ r8] c'2^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 r8[ c'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 r8[ cis'8^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ cis'2 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2 r2  }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { cis'2 r8[ b8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ b4 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b8[ r8] r2  ais4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { ais2. ~ ais8[ r8] }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b4 ~ b8[ r8] r4 r8[ ais8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 r8[ c'8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] r4 r8[ b8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b4 r8[ b8^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }}] ~ b2 ~ }
 \bar "|"  
 { b2. r8[ cis'8^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'8[ r8] r2.  }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 r2  }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { cis'2 r8[ dis'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. ~ dis'8[ r8] }
 \bar "|"  
 { r1  }
 \bar "|"  
 { cis'1^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { cis'2 ~ cis'8[ r8] d'4^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 r2  dis'4^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 }
\bar "|."
}