{  
 { f'1^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. r8[ f'8^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'8[ fis'8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ fis'2. ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'4 r4 r8[ fis'8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ fis'4 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 r2  }
 \bar "|"  
 { r4 fis'2.^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { fis'2 ~ fis'8[ g'8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ g'4 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'8[ gis'8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ gis'2. ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'2 r2  }
 \bar "|"  
 { r4 gis'2.^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'4 ~ gis'8[ gis'8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ gis'2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'2. r4 }
 \bar "|"  
 { r8[ a'8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ a'2. ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'2. r8[ b'8^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'2 f'2^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { f'1 }
 \bar "|"  
 { fis'1^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'4 ~ fis'8[ r8] r8[ g'8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ g'4 ~ }
 \bar "|"  
 { g'1 }
 \bar "|"  
 { g'1^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'2 r2  }
 \bar "|"  
 { r4 r8[ g'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ g'2 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'4 r8[ g'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ g'2 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'4 r2  r8[ g'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'2. ~ g'8[ gis'8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'4 ~ gis'8[ r8] r2  }
 \bar "|"  
 { gis'1^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'4 ~ gis'8[ r8] r2  }
 \bar "|"  
 { r8[ dis'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ dis'2. ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] r8[ dis'8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] r8[ e'8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ e'4 ~ }
 \bar "|"  
 { e'2. ~ e'8[ f'8^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. ~ f'8[ r8] }
 \bar "|"  
 { r4 f'2.^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { f'2. fis'4^\markup { \pad-markup #0.2 "-19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 }
 \bar "|"  
 { g'1^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'2. ~ g'8[ g'8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'4 ~ g'8[ gis'8^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ gis'2 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'1 ~ }
 \bar "|"  
 { gis'2 a'2^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'2. ~ a'8[ r8] }
 \bar "|"  
 { r4 a'2.^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'2 r2  }
 \bar "|"  
 { r8[ a'8^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ a'2. ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'1 ~ }
 \bar "|"  
 { a'8[ ais'8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ ais'2. ~ }
 \bar "|"  
 { ais'2. b'4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { b'1 ~ }
 \bar "|"  
 { b'1 }
 \bar "|"  
 { r4 r8[ b'8^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ b'2 ~ }
 \bar "|"  
 { b'2. c''4^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''4 r2  c''4^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 }
 \bar "|"  
 { r2.  c''4^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''1 }
 \bar "|"  
 { r4 r8[ c''8^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ c''2 ~ }
 \bar "|"  
 { c''1 ~ }
 \bar "|"  
 { c''2 r2  }
 \bar "|"  
 { r4 r8[ cis''8^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ cis''2 ~ }
 \bar "|"  
 { cis''1 }
 \bar "|"  
 { cis''1^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { cis''2 d''2^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''2. r4 }
 \bar "|"  
 { d''1^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 ~ }
 \bar "|"  
 { d''1 }
 \bar "|"  
 { r4 r8[ dis''8^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ dis''2 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''2. ~ dis''8[ r8] }
 \bar "|"  
 { r4 dis''2.^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { dis''2. e''4^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''4 ~ e''8[ f''8^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ f''2 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''1 }
 \bar "|"  
 { fis''1^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''4 ~ fis''8[ r8] r4 fis''4^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 }
 \bar "|"  
 { r4 r8[ c''8^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ c''2 ~ }
 \bar "|"  
 { c''2. ~ c''8[ cis''8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''2 ~ cis''8[ cis''8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ cis''4 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''4 r4 r8[ cis''8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ cis''4 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''1 ~ }
 \bar "|"  
 { cis''4 ~ cis''8[ r8] r2  }
 \bar "|"  
 { cis''1^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { cis''4 ~ cis''8[ d''8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ d''2 ~ }
 \bar "|"  
 { d''2. ~ d''8[ dis''8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''2 r2  }
 \bar "|"  
 { r4 dis''2.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 ~ }
 \bar "|"  
 { dis''1 }
 \bar "|"  
 { r2.  e''4^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 }
 \bar "|"  
 { r2  r8[ e''8^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ e''4 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''2. ~ e''8[ r8] }
 \bar "|"  
 { r8[ e''8^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ e''2. ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''2. ~ e''8[ r8] }
 \bar "|"  
 { r2  r8[ e''8^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ e''4 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''1 ~ }
 \bar "|"  
 { e''2 r2  }
 \bar "|"  
 { r8[ f''8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ f''2. ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''1 ~ }
 \bar "|"  
 { f''4 ~ f''8[ fis''8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ fis''2 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''1 ~ }
 \bar "|"  
 { fis''2. g''4^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''4 g''2.^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''2 ~ g''8[ r8] r4 }
 \bar "|"  
 { r2  g''2^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''8[ r8] r4 g''2^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''2. ~ g''8[ r8] }
 \bar "|"  
 { r4 r8[ g''8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ g''2 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''1 ~ }
 \bar "|"  
 { g''2. ~ g''8[ gis''8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { gis''1 ~ }
 \bar "|"  
 { gis''1 ~ }
 \bar "|"  
 { gis''2. r4 }
 \bar "|"  
 { r8[ gis''8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ gis''2. ~ }
 \bar "|"  
 { gis''1 ~ }
 \bar "|"  
 { gis''1 ~ }
 \bar "|"  
 { gis''1 ~ }
 \bar "|"  
 { gis''1 ~ }
 \bar "|"  
 { gis''1 ~ }
 \bar "|"  
 { gis''4 ~ gis''8[ a''8^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ a''2 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''1 ~ }
 \bar "|"  
 { a''1 }
 \bar "|"  
 { r2  r8[ ais''8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ ais''4 ~ }
 \bar "|"  
 { ais''1 ~ }
 \bar "|"  
 { ais''8[ b''8^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ b''2. ~ }
 \bar "|"  
 { b''1 ~ }
 \bar "|"  
 { b''1 ~ }
 \bar "|"  
 { b''4 r2  b''4^\markup { \pad-markup #0.2 "+22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { b''1 ~ }
 \bar "|"  
 { b''8[ c'''8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ c'''2. ~ }
 \bar "|"  
 { c'''1 ~ }
 \bar "|"  
 { c'''1 }
 \bar "|"  
 { r1 }
\bar "|."
}