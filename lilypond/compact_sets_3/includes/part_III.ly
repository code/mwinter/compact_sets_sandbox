{  
 { a,1^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { a,4 ais,2.^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2. r8[ ais,8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,8[ r8] r4 r8[ b,8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ b,4 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,8[ c8^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ c2. ~ }
 \bar "|"  
 { c4 ~ c8[ cis8^\markup { \pad-markup #0.2 "-26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ cis2 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 }
 \bar "|"  
 { r4 r8[ g,8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ g,2 ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { gis,1^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { gis,2 gis,2^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2. ~ gis,8[ r8] }
 \bar "|"  
 { r2  a,2^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 r2  }
 \bar "|"  
 { r4 a,2.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 }
 \bar "|"  
 { r2.  ais,4^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2 ~ ais,8[ r8] r4 }
 \bar "|"  
 { r8[ b,8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ b,2. ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,4 ~ b,8[ r8] r2  }
 \bar "|"  
 { r8[ c8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ c2. ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c2 r2  }
 \bar "|"  
 { r4 cis2.^\markup { \pad-markup #0.2 "-26"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis4 ~ cis8[ r8] r4 cis4^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis2. r4 }
 \bar "|"  
 { r8[ cis8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ cis2. ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis2 ~ cis8[ r8] r8[ cis8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis2. ~ cis8[ r8] }
 \bar "|"  
 { r4 d2.^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { d2 ~ d8[ a,8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ a,4 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 ~ a,8[ r8] r4 }
 \bar "|"  
 { r4 a,2.^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { a,1 }
 \bar "|"  
 { ais,1^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { ais,2. b,4^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,4 ~ b,8[ r8] r2  }
 \bar "|"  
 { r8[ c8^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ c2. ~ }
 \bar "|"  
 { c2. ~ c8[ cis8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis4 d2.^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d2 r2  }
 \bar "|"  
 { r4 r8[ a,8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ a,2 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,4 ~ a,8[ r8] r4 r8[ ais,8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2 b,2^\markup { \pad-markup #0.2 "-39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,8[ r8] r2  r8[ b,8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,2. ~ b,8[ c8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c2 c2^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c2. ~ c8[ r8] }
 \bar "|"  
 { r2  r8[ cis8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ cis4 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis4 ~ cis8[ d8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ d2 ~ }
 \bar "|"  
 { d2 ~ d8[ d8^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ d4 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d4 a,2.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,4 ~ a,8[ r8] r4 r8[ ais,8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,4 b,2.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,8[ r8] r2.  }
 \bar "|"  
 { b,1^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,2 r2  }
 \bar "|"  
 { b,1^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,2. f,4^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,8[ fis,8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ fis,2. ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,4 r2.  }
 \bar "|"  
 { g,1^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { g,2 g,2^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { g,2. gis,4^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 ~ gis,8[ r8] r2  }
 \bar "|"  
 { gis,1^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,8[ a,8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ a,2. ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2. ~ a,8[ r8] }
 \bar "|"  
 { r4 ais,2.^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2 r4 b,4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 }
 \bar "|"  
 { r2  c2^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { c1 }
 \bar "|"  
 { c1^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { c2. d4^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 }
 \bar "|"  
 { d1^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { d2 a,2^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 ~ a,8[ r8] r4 }
 \bar "|"  
 { r2  ais,2^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { ais,2. ~ ais,8[ b,8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,2. ~ b,8[ r8] }
 \bar "|"  
 { r8[ b,8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ b,2. ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,8[ r8] r2  cis4^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis2. ~ cis8[ r8] }
 \bar "|"  
 { r2  ais,2^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { ais,1 }
 \bar "|"  
 { b,1^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,8[ ais,8^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ ais,2. ~ }
 \bar "|"  
 { ais,1 }
 \bar "|"  
 { r2.  r8[ c8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c4 ~ c8[ r8] c2^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c8[ r8] r2  r8[ cis8^\markup { \pad-markup #0.2 "+6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis1 ~ }
 \bar "|"  
 { cis2. d4^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d4 ~ d8[ r8] r2  }
 \bar "|"  
 { r8[ dis8^\markup { \pad-markup #0.2 "-29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ dis2. ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis2. ~ dis8[ r8] }
 \bar "|"  
 { r4 r8[ e8^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ e2 ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e8[ f8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ f2. ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f2. ~ f8[ r8] }
 \bar "|"  
 { r2.  f4^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f2 ~ f8[ r8] r4 }
 \bar "|"  
 { r4 fis2.^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { fis2 ~ fis8[ fis8^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ fis4 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis2. ~ fis8[ r8] }
 \bar "|"  
 { g1^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { g2 ~ g8[ g8^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ g4 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g4 ~ g8[ gis8^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ gis2 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis2 ~ gis8[ r8] r4 }
 \bar "|"  
 { r4 r8[ a8^\markup { \pad-markup #0.2 "-38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ a2 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 }
 \bar "|"  
 { a1^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { a2 ~ a8[ ais8^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 b2.^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b8[ r8] r4 }
 \bar "|"  
 { r2  b2^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 r2  }
 \bar "|"  
 { r4 c'2.^\markup { \pad-markup #0.2 "+13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { c'2 ~ c'8[ fis8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ fis4 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis2. ~ fis8[ r8] }
 \bar "|"  
 { r4 g2.^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g8[ r8] r4 r8[ g8^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ g4 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g2 gis2^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis2 ~ gis8[ r8] r4 }
 \bar "|"  
 { r4 r8[ a8^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }}] ~ a2 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2 r2  }
 \bar "|"  
 { r4 dis2.^\markup { \pad-markup #0.2 "-11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { dis1 ~ }
 \bar "|"  
 { dis1 }
 \bar "|"  
 { e1^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e1 ~ }
 \bar "|"  
 { e4 r4 r8[ f8^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ f4 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f8[ f8^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ f2. ~ }
 \bar "|"  
 { f2 ~ f8[ fis8^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ fis4 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis4 ~ fis8[ r8] r8[ fis8^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ fis4 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis2 ~ fis8[ r8] r4 }
 \bar "|"  
 { r4 g2.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g2 r4 g4^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { g1 ~ }
 \bar "|"  
 { g8[ gis8^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ gis2. ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 }
 \bar "|"  
 { r2  a2^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a4 ais2.^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { ais1 }
 \bar "|"  
 { ais1^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais8[ r8] r2.  }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { b4 ~ b8[ f8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ f2 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f1 ~ }
 \bar "|"  
 { f2 r2  }
 \bar "|"  
 { r8[ fis8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ fis2. ~ }
 \bar "|"  
 { fis2. fis4^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis2. ~ fis8[ r8] }
 \bar "|"  
 { r2.  fis4^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis1 ~ }
 \bar "|"  
 { fis2. ~ fis8[ r8] }
 \bar "|"  
 { r2  r8[ g8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ g4 ~ }
 \bar "|"  
 { g2. ~ g8[ gis8^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis1 ~ }
 \bar "|"  
 { gis2. r4 }
 \bar "|"  
 { r8[ gis8^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ gis2. ~ }
 \bar "|"  
 { gis2. a4^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2. ~ a8[ r8] }
 \bar "|"  
 { r2  r8[ a8^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ a4 ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a4 ~ a8[ ais8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ ais2 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 ~ ais8[ r8] r8[ ais8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ ais4 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais8[ ais8^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ ais2. ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais4 ~ ais8[ r8] b2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { b2. ~ b8[ b8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b8[ r8] r4 }
 \bar "|"  
 { r2  cis'2^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 d'2.^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { d'2. ~ d'8[ d'8^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 }
 \bar "|"  
 { r4 dis'2.^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ r8] r4 }
 \bar "|"  
 { r2  ais2^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2. ~ ais8[ r8] }
 \bar "|"  
 { r8[ ais8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ ais2. ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 r2  }
 \bar "|"  
 { r4 ais2.^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { ais2 b2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b8[ r8] r4 c'2^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] cis'2^\markup { \pad-markup #0.2 "-28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { d'1^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 r4 d'2^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'8[ r8] r2  r8[ a8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { a1 ~ }
 \bar "|"  
 { a2 ais2^\markup { \pad-markup #0.2 "-17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais1 ~ }
 \bar "|"  
 { ais2 r2  }
 \bar "|"  
 { r8[ b8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ b2. ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b8[ r8] r4 }
 \bar "|"  
 { r4 b2.^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { b2. ~ b8[ b8^\markup { \pad-markup #0.2 "-5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b1 ~ }
 \bar "|"  
 { b2 ~ b8[ r8] r4 }
 \bar "|"  
 { b1^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { b2 ~ b8[ c'8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ c'4 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'4 ~ c'8[ r8] r4 cis'4^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r2. }
\bar "|."
}