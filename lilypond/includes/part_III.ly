{  
 { a,1^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,8[ r8] r8[ gis,8^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ gis,2 ~ }
 \bar "|"  
 { gis,2 ~ gis,8[ r8] ais,4^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2. ~ ais,8[ r8] }
 \bar "|"  
 { r2.  r8[ ais,8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,4 ~ ais,8[ r8] gis,2^\markup { \pad-markup #0.2 "+34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2. ~ gis,8[ r8] }
 \bar "|"  
 { r2  r8[ ais,8^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ ais,4 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2 ~ ais,8[ r8] r4 }
 \bar "|"  
 { r4 r8[ b,8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ b,2 ~ }
 \bar "|"  
 { b,2. r8[ ais,8^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,4 r8[ gis,8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ gis,2 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 r8[ ais,8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ ais,2 ~ }
 \bar "|"  
 { ais,2. r8[ a,8^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2. ~ a,8[ r8] }
 \bar "|"  
 { r2.  ais,4^\markup { \pad-markup #0.2 "+28"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,8[ r8] gis,2.^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 ~ gis,8[ r8] r4 }
 \bar "|"  
 { r2  g,2^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,8[ r8] r4 r8[ gis,8^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ gis,4 ~ }
 \bar "|"  
 { gis,2. ~ gis,8[ r8] }
 \bar "|"  
 { a,1^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 }
 \bar "|"  
 { r2  g,2^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2 ~ g,8[ r8] r4 }
 \bar "|"  
 { r4 r8[ gis,8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ gis,2 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 }
 \bar "|"  
 { r2  g,2^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 ~ gis,8[ r8] r4 }
 \bar "|"  
 { a,1^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { a,4 ~ a,8[ r8] ais,2^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2. ~ ais,8[ r8] }
 \bar "|"  
 { r2  gis,2^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { gis,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 ~ gis,8[ r8] ais,4^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2 r2  }
 \bar "|"  
 { r2  a,2^\markup { \pad-markup #0.2 "-27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,8[ r8] g,2.^\markup { \pad-markup #0.2 "+18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { g,2 ~ g,8[ r8] gis,4^\markup { \pad-markup #0.2 "+42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 r4 r8[ gis,8^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 }
 \bar "|"  
 { r4 r8[ fis,8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ fis,2 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,8[ r8] r4 fis,2^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,4 r8[ g,8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ g,2 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 r2.  }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,2 r8[ a,8^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ a,4 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 }
 \bar "|"  
 { r2.  gis,4^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 ~ gis,8[ r8] g,2^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2 r2  }
 \bar "|"  
 { r8[ g,8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ g,2. ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2. r4 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "+13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2. ~ gis,8[ r8] }
 \bar "|"  
 { r2  g,2^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "+13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,2 r8[ a,8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ a,4 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 r8[ ais,8^\markup { \pad-markup #0.2 "-9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ ais,4 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2 r2  }
 \bar "|"  
 { r4 r8[ a,8^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ a,2 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2. r4 }
 \bar "|"  
 { r4 r8[ ais,8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ ais,2 ~ }
 \bar "|"  
 { ais,2 r8[ gis,8^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ gis,4 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,8[ r8] fis,2.^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,8[ r8] gis,2.^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2. r4 }
 \bar "|"  
 { r2.  g,4^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 ~ g,8[ r8] r2  }
 \bar "|"  
 { gis,1^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { gis,2. r4 }
 \bar "|"  
 { r2  r8[ gis,8^\markup { \pad-markup #0.2 "-50"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ gis,4 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2. r8[ g,8^\markup { \pad-markup #0.2 "-3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 ~ g,8[ r8] r2  }
 \bar "|"  
 { r8[ a,8^\markup { \pad-markup #0.2 "-48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ a,2. ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2. r4 }
 \bar "|"  
 { r2  g,2^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { g,2. ~ g,8[ r8] }
 \bar "|"  
 { g,1^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { g,2. r4 }
 \bar "|"  
 { r2  g,2^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 ~ gis,8[ r8] ais,2^\markup { \pad-markup #0.2 "-24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2. r4 }
 \bar "|"  
 { r2  gis,2^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { gis,2. ~ gis,8[ r8] }
 \bar "|"  
 { g,1^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 r2  gis,4^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 r4 r8[ g,8^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ g,4 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,8[ r8] gis,2.^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 }
 \bar "|"  
 { r2  gis,2^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 ~ gis,8[ r8] r4 }
 \bar "|"  
 { r4 g,2.^\markup { \pad-markup #0.2 "+31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2 ~ g,8[ r8] r4 }
 \bar "|"  
 { r2  r8[ gis,8^\markup { \pad-markup #0.2 "+43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ gis,4 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 ~ gis,8[ r8] r4 g,4^\markup { \pad-markup #0.2 "-22"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2 r8[ f,8^\markup { \pad-markup #0.2 "+13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ f,4 ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,2 r8[ fis,8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ fis,4 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,2. ~ fis,8[ r8] }
 \bar "|"  
 { r2.  r8[ f,8^\markup { \pad-markup #0.2 "+13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,4 r8[ fis,8^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ fis,2 ~ }
 \bar "|"  
 { fis,2. r8[ gis,8^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2. r4 }
 \bar "|"  
 { r4 fis,2.^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,4 r4 r8[ gis,8^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ gis,4 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 r8[ a,8^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ a,2 ~ }
 \bar "|"  
 { a,2 r8[ a,8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }}] ~ a,4 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,4 ~ a,8[ r8] r4 r8[ gis,8^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 }
 \bar "|"  
 { r8[ a,8^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ a,2. ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 r2  }
 \bar "|"  
 { r4 g,2.^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { g,2. r8[ fis,8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,8[ r8] g,2.^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r2.  r8[ fis,8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,8[ r8] g,2.^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2 ~ g,8[ r8] r4 }
 \bar "|"  
 { r4 r8[ fis,8^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ fis,2 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,4 ~ fis,8[ r8] gis,2^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { gis,1 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { gis,1^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 r4 g,4^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 ~ g,8[ r8] fis,2^\markup { \pad-markup #0.2 "+32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 }
 \bar "|"  
 { r8[ f,8^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ f,2. ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,1 }
 \bar "|"  
 { r4 r8[ g,8^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ g,2 ~ }
 \bar "|"  
 { g,2 ~ g,8[ r8] fis,4^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,2. r4 }
 \bar "|"  
 { r2  gis,2^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { gis,2. r8[ ais,8^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,2 ~ ais,8[ r8] r4 }
 \bar "|"  
 { r4 gis,2.^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { gis,4 ~ gis,8[ r8] a,2^\markup { \pad-markup #0.2 "-9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { a,2. r8[ a,8^\markup { \pad-markup #0.2 "+44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 }
 \bar "|"  
 { r2.  gis,4^\markup { \pad-markup #0.2 "-21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 r8[ a,8^\markup { \pad-markup #0.2 "-9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ a,2 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 r2  }
 \bar "|"  
 { gis,1^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,8[ r8] r4 g,2^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 r4 r8[ f,8^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ f,4 ~ }
 \bar "|"  
 { f,1 }
 \bar "|"  
 { r8[ g,8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ g,2. ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 r2  r8[ g,8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { f,1^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,2. r8[ fis,8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,2 r4 r8[ fis,8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,4 r8[ gis,8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↓" }}] ~ gis,2 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,8[ r8] r2  fis,4^\markup { \pad-markup #0.2 "+41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { fis,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 ~ gis,8[ r8] r4 r8[ g,8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,4 r2  r8[ f,8^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { f,1 ~ }
 \bar "|"  
 { f,4 r8[ g,8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }}] ~ g,2 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r2  g,2^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2 ~ g,8[ r8] r4 }
 \bar "|"  
 { r4 r8[ gis,8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ gis,2 ~ }
 \bar "|"  
 { gis,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 r2.  }
 \bar "|"  
 { r8[ g,8^\markup { \pad-markup #0.2 "-16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ g,2. ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2. r4 }
 \bar "|"  
 { r8[ fis,8^\markup { \pad-markup #0.2 "-13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ fis,2. ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,1 ~ }
 \bar "|"  
 { fis,2. r4 }
 \bar "|"  
 { f,1^\markup { \pad-markup #0.2 "-25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { f,2 r8[ fis,8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ fis,4 ~ }
 \bar "|"  
 { fis,2. ~ fis,8[ r8] }
 \bar "|"  
 { g,1^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { g,1 }
 \bar "|"  
 { r2.  a,4^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 r4 r8[ g,8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,1 ~ }
 \bar "|"  
 { g,2 ~ g,8[ r8] r4 }
 \bar "|"  
 { r4 a,2.^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,4 ~ a,8[ r8] r4 r8[ a,8^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 }
 \bar "|"  
 { r8[ gis,8^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ gis,2. ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,4 r4 fis,2^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { fis,2. ~ fis,8[ r8] }
 \bar "|"  
 { gis,1^\markup { \pad-markup #0.2 "-8"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 r4 r8[ a,8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2. r8[ gis,8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { gis,1 ~ }
 \bar "|"  
 { gis,2 ~ gis,8[ r8] r4 }
 \bar "|"  
 { r2  ais,2^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { ais,1 ~ }
 \bar "|"  
 { ais,1 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { a,1^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { a,1 ~ }
 \bar "|"  
 { a,2 r2  }
 \bar "|"  
 { ais,1^\markup { \pad-markup #0.2 "+23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { ais,2 r8[ b,8^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 3↓" }}] ~ b,4 ~ }
 \bar "|"  
 { b,1 ~ }
 \bar "|"  
 { b,1 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { c1^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c1 ~ }
 \bar "|"  
 { c4 r2  d4^\markup { \pad-markup #0.2 "-47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 ~ }
 \bar "|"  
 { d1 }
 \bar "|"  
 { r1 }
\bar "|."
}