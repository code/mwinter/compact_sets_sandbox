{  
 { f'1^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 r8[ g'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ g'2 ~ }
 \bar "|"  
 { g'2 ~ g'8[ r8] f'4^\markup { \pad-markup #0.2 "+14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. ~ f'8[ r8] }
 \bar "|"  
 { r4 fis'2.^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { fis'2. ~ fis'8[ r8] }
 \bar "|"  
 { r2.  r8[ f'8^\markup { \pad-markup #0.2 "-14"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 ~ f'8[ r8] fis'2^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'8[ r8] r2.  }
 \bar "|"  
 { f'1^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. r4 }
 \bar "|"  
 { r4 r8[ f'8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ f'2 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 ~ f'8[ r8] f'2^\markup { \pad-markup #0.2 "+9"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2 r2  }
 \bar "|"  
 { r8[ e'8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ e'2. ~ }
 \bar "|"  
 { e'2 r8[ f'8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ f'4 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 r2.  }
 \bar "|"  
 { r8[ dis'8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ dis'2. ~ }
 \bar "|"  
 { dis'4 r8[ e'8^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ e'2 ~ }
 \bar "|"  
 { e'2. r8[ fis'8^\markup { \pad-markup #0.2 "+10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 r2  }
 \bar "|"  
 { r4 r8[ f'8^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ f'2 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2 ~ f'8[ r8] r4 }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. r8[ e'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 }
 \bar "|"  
 { r2.  dis'4^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ r8] d'4^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 r4 r8[ cis'8^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ cis'4 ~ }
 \bar "|"  
 { cis'2. ~ cis'8[ r8] }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { dis'2 r8[ dis'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 r4 e'2^\markup { \pad-markup #0.2 "+29"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 }
 \bar "|"  
 { r2  f'2^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { f'2. ~ f'8[ r8] }
 \bar "|"  
 { f'1^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { f'1 }
 \bar "|"  
 { r2  r8[ dis'8^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ r8] r4 }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. r4 }
 \bar "|"  
 { r2  f'2^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2 ~ f'8[ r8] fis'4^\markup { \pad-markup #0.2 "+11"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 r2  }
 \bar "|"  
 { r4 r8[ f'8^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ f'2 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2 r2  }
 \bar "|"  
 { r2  fis'2^\markup { \pad-markup #0.2 "+7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'8[ r8] f'2.^\markup { \pad-markup #0.2 "-44"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2 ~ f'8[ r8] r8[ fis'8^\markup { \pad-markup #0.2 "+21"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 r8[ f'8^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ f'4 ~ }
 \bar "|"  
 { f'2. ~ f'8[ r8] }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. ~ e'8[ r8] }
 \bar "|"  
 { d'1^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { d'2. r4 }
 \bar "|"  
 { r2  r8[ e'8^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ e'4 ~ }
 \bar "|"  
 { e'2. r8[ d'8^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 }
 \bar "|"  
 { r2  e'2^\markup { \pad-markup #0.2 "-10"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e'2 ~ e'8[ r8] d'4^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 r2.  }
 \bar "|"  
 { r8[ dis'8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ dis'2. ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. ~ dis'8[ r8] }
 \bar "|"  
 { r2.  dis'4^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { dis'1 }
 \bar "|"  
 { r8[ dis'8^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ dis'2. ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] e'2^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 r2  }
 \bar "|"  
 { r8[ fis'8^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ fis'2. ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 r8[ e'8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ e'4 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. ~ e'8[ r8] }
 \bar "|"  
 { r2  f'2^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'1 }
 \bar "|"  
 { r8[ fis'8^\markup { \pad-markup #0.2 "+48"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ fis'2. ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 ~ fis'8[ r8] r4 }
 \bar "|"  
 { f'1^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. ~ f'8[ r8] }
 \bar "|"  
 { r4 r8[ f'8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ f'2 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'8[ r8] e'2.^\markup { \pad-markup #0.2 "+35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'8[ r8] r2  e'4^\markup { \pad-markup #0.2 "-32"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'8[ r8] f'2.^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 }
 \bar "|"  
 { r2.  fis'4^\markup { \pad-markup #0.2 "+0"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2. r4 }
 \bar "|"  
 { r2  r8[ g'8^\markup { \pad-markup #0.2 "-3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ g'4 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'8[ r8] f'2.^\markup { \pad-markup #0.2 "+19"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { f'2. r8[ fis'8^\markup { \pad-markup #0.2 "-15"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'4 r4 f'2^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 r2.  }
 \bar "|"  
 { r8[ f'8^\markup { \pad-markup #0.2 "-34"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ f'2. ~ }
 \bar "|"  
 { f'2 ~ f'8[ r8] e'4^\markup { \pad-markup #0.2 "-46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. r4 }
 \bar "|"  
 { r2  f'2^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'2. ~ f'8[ r8] }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { e'2. r4 }
 \bar "|"  
 { r2  e'2^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 ~ e'8[ r8] dis'4^\markup { \pad-markup #0.2 "-40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 r2.  }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. ~ e'8[ r8] }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. r4 }
 \bar "|"  
 { r2  e'2^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { e'2. ~ e'8[ r8] }
 \bar "|"  
 { fis'1^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'8[ r8] r4 r8[ fis'8^\markup { \pad-markup #0.2 "+12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ fis'4 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 r8[ f'8^\markup { \pad-markup #0.2 "-43"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ f'4 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'8[ r8] r4 dis'2^\markup { \pad-markup #0.2 "+45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] r2  }
 \bar "|"  
 { r4 f'2.^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. r4 }
 \bar "|"  
 { r2  r8[ e'8^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ e'4 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 r4 f'4^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'8[ r8] e'2.^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e'2 r8[ e'8^\markup { \pad-markup #0.2 "-42"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ e'4 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'8[ r8] r2.  }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { dis'2 r8[ d'8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ d'4 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 }
 \bar "|"  
 { r2.  r8[ e'8^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. r4 }
 \bar "|"  
 { r4 e'2.^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'4 r4 r8[ d'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ d'4 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 ~ d'8[ r8] r4 r8[ d'8^\markup { \pad-markup #0.2 "+36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 ~ d'8[ r8] e'2^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 r2  }
 \bar "|"  
 { r4 f'2.^\markup { \pad-markup #0.2 "-45"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { f'2. r8[ dis'8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'8[ r8] dis'2.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. ~ dis'8[ r8] }
 \bar "|"  
 { r2.  r8[ dis'8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'8[ r8] dis'2.^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ r8] r4 }
 \bar "|"  
 { r4 r8[ dis'8^\markup { \pad-markup #0.2 "-49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ dis'2 ~ }
 \bar "|"  
 { dis'1 }
 \bar "|"  
 { r8[ cis'8^\markup { \pad-markup #0.2 "+39"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ cis'2. ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r8[ c'8^\markup { \pad-markup #0.2 "+27"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↑" }}] ~ c'2 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'1 ~ }
 \bar "|"  
 { c'2 r4 cis'4^\markup { \pad-markup #0.2 "+3"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { r8[ c'8^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ c'2. ~ }
 \bar "|"  
 { c'2 ~ c'8[ r8] d'4^\markup { \pad-markup #0.2 "-12"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2 r2  }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] f'2^\markup { \pad-markup #0.2 "-18"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 ~ f'8[ r8] r2  }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 ~ e'8[ r8] r8[ d'8^\markup { \pad-markup #0.2 "+46"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { d'1 }
 \bar "|"  
 { r8[ e'8^\markup { \pad-markup #0.2 "-30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }}] ~ e'2. ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 }
 \bar "|"  
 { r2  r8[ dis'8^\markup { \pad-markup #0.2 "+4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ r8] f'4^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'4 r8[ e'8^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ e'2 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 ~ e'8[ r8] r4 }
 \bar "|"  
 { r2  e'2^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { e'2. r8[ d'8^\markup { \pad-markup #0.2 "+38"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2 r2  }
 \bar "|"  
 { r4 e'2.^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'4 ~ e'8[ r8] r2  }
 \bar "|"  
 { f'1^\markup { \pad-markup #0.2 "+5"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'8[ r8] r4 e'2^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { e'2. r8[ dis'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] r4 r8[ e'8^\markup { \pad-markup #0.2 "+24"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }}] ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 r8[ dis'8^\markup { \pad-markup #0.2 "-31"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 }
 \bar "|"  
 { r1  }
 \bar "|"  
 { e'1^\markup { \pad-markup #0.2 "-7"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { e'4 ~ e'8[ r8] dis'2^\markup { \pad-markup #0.2 "-41"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 7↑" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] r4 r8[ cis'8^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 }
 \bar "|"  
 { r8[ cis'8^\markup { \pad-markup #0.2 "-6"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 3↓" }}] ~ cis'2. ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r2  d'4^\markup { \pad-markup #0.2 "+13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 5↑" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'4 ~ d'8[ r8] r4 r8[ dis'8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 r2  r8[ e'8^\markup { \pad-markup #0.2 "+17"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }}] ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'4 r8[ e'8^\markup { \pad-markup #0.2 "-36"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↓" }}] ~ e'2 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2. ~ e'8[ r8] }
 \bar "|"  
 { r2  dis'2^\markup { \pad-markup #0.2 "-2"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'4 ~ dis'8[ r8] cis'2^\markup { \pad-markup #0.2 "+47"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'4 r2.  }
 \bar "|"  
 { r8[ d'8^\markup { \pad-markup #0.2 "+13"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ d'2. ~ }
 \bar "|"  
 { d'2 r8[ cis'8^\markup { \pad-markup #0.2 "+33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }}] ~ cis'4 ~ }
 \bar "|"  
 { cis'1 ~ }
 \bar "|"  
 { cis'2. ~ cis'8[ r8] }
 \bar "|"  
 { r8[ dis'8^\markup { \pad-markup #0.2 "-33"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }}] ~ dis'2. ~ }
 \bar "|"  
 { dis'4 r8[ d'8^\markup { \pad-markup #0.2 "+1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ d'2 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2 r2  }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "+20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2. ~ dis'8[ r8] }
 \bar "|"  
 { d'1^\markup { \pad-markup #0.2 "-35"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 7↓" }} ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'1 ~ }
 \bar "|"  
 { d'2. ~ d'8[ r8] }
 \bar "|"  
 { dis'1^\markup { \pad-markup #0.2 "+16"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↓" }} ~ }
 \bar "|"  
 { dis'1 ~ }
 \bar "|"  
 { dis'2 ~ dis'8[ r8] r8[ e'8^\markup { \pad-markup #0.2 "+49"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }}] ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 ~ e'8[ r8] r4 }
 \bar "|"  
 { r4 e'2.^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'4 r2  r8[ e'8^\markup { \pad-markup #0.2 "-4"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }}] ~ }
 \bar "|"  
 { e'1 ~ }
 \bar "|"  
 { e'2 r8[ dis'8^\markup { \pad-markup #0.2 "-1"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }}] ~ dis'4 ~ }
 \bar "|"  
 { dis'1 }
 \bar "|"  
 { r8[ f'8^\markup { \pad-markup #0.2 "-23"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }}] ~ f'2. ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 }
 \bar "|"  
 { r2  f'2^\markup { \pad-markup #0.2 "+30"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { f'2. ~ f'8[ r8] }
 \bar "|"  
 { g'1^\markup { \pad-markup #0.2 "-20"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↑" }} ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'1 ~ }
 \bar "|"  
 { g'4 r8[ f'8^\markup { \pad-markup #0.2 "+25"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 5↓" }}] ~ f'2 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'2. r4 }
 \bar "|"  
 { r2  fis'2^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "II"\normal-size-super " 3↓" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 r2  }
 \bar "|"  
 { fis'1^\markup { \pad-markup #0.2 "+37"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "I"\normal-size-super " 1" }} ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'1 ~ }
 \bar "|"  
 { fis'2 ~ fis'8[ r8] f'4^\markup { \pad-markup #0.2 "+40"}_\markup {  \lower #3 \pad-markup #0.2 \concat{ "III"\normal-size-super " 11↑" }} ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 ~ }
 \bar "|"  
 { f'1 }
 \bar "|"  
 { r1 }
\bar "|."
}